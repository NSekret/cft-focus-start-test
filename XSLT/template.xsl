<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:template match="/">
        <sources>
            <xsl:variable name="allItems" select="items/item"/>
            <xsl:for-each-group select="$allItems" group-by="@source">
                <source>
                    <xsl:copy-of select="@source"/>
                    <xsl:variable name="sourceName" select="@source"/>
                    <xsl:for-each select="$allItems">
                        <xsl:if test="@source=$sourceName">
                            <item>
                                <xsl:copy-of select="@source"/>
                                <xsl:copy-of select="@name"/>
                            </item>
                        </xsl:if>
                    </xsl:for-each>
                </source>
            </xsl:for-each-group>
        </sources>
    </xsl:template>
</xsl:stylesheet>