var destroy_session = function () {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send("destroy=true");
    };

//При смены окна/закрытии браузера/обновлении вкладки отправляем асинхронный запрос для уничтожения сессии на сервере
window.onblur = destroy_session;
window.onbeforeunload = destroy_session;

var formGet = document.getElementById('get-form');
var formPost = document.getElementById('post-form');

var disable_onbeforeunload = function () {
    window.onbeforeunload = null;
};

//При отправке формы отклчаем событие onbeforeunload, чтобы сессия не уничтожилась
formGet.addEventListener("submit", disable_onbeforeunload);
formPost.addEventListener("submit", disable_onbeforeunload);