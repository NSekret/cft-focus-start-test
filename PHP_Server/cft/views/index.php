<form id="get-form">
    <label for="input-id">Enter string:</label>
    <input type="text" name="input" id="input-id" required>
    <p>
        <button type="submit">Submit form a GET request</button>
    </p>
</form>

<br>

<form id="post-form" method="post">
    <label for="input-id">Enter string:</label>
    <input type="text" name="input" id="input-id" required>
    <p>
        <button type="submit">Submit form a POST request</button>
    </p>
</form>

<?php if($args['detected'] !== NULL) : ?>
    <?php if($args['detected'] === TRUE) : ?>
        <h1>Received string contains a digits!</h1>
    <?php else : ?>
        <h1>Received string not contains a digits!</h1>
    <?php endif ?>
<?php endif ?>

<h1>Statistic</h1>
<h3>Strings count that contain digits: <?= $args['containing'] ?></h3>
<h3>Strings count that not contain digits: <?= $args['not_containing'] ?></h3>

<script src="/js/destroy-session.js"></script>

