<?php

/** Инициализация сессии */
function init_session()
{
	session_start(); //инициализация сессии для хранения статистики

	//Проверям наличие переменных в сессии, если отсутствуют, устанавливаем их значения в 0
	if(array_key_exists('containing', $_SESSION) === FALSE)
	{
		$_SESSION['containing'] = 0;
	}
	if(array_key_exists('not_containing', $_SESSION) === FALSE)
	{
		$_SESSION['not_containing'] = 0;
	}
}

/** Уничтожение сессии */
function destroy_session()
{
	session_destroy();
}

/** Формируем предстваление(рендерим шаблон) */
function render($view, $args = null)
{
	if (is_array($args) && ! empty($vars))
	{
		extract($args); //импортируем переменные для рендера шаблона
	}

	ob_start(); //осуществляем вывод во внутренний буфер(чтобы ответ сразу не был отдан браузеру)
	include($view);
	return ob_get_clean();//овзвращаем отрендеренный шаблон
}

/** Проверяет наличие цифр в строке */
function check_for_number($str)
{
	/** проходим по всей строке */
	for($i = 0; $i < strlen($str); $i++)
	{
		if (is_numeric($str[$i])) //проверяем является ли символ цифрой
		{
			return true;
		}
	}
	return false;
}

function check()
{
	$detected = null;
	$input = null;
	switch($_SERVER['REQUEST_METHOD'])
	{
		case "GET":
			if(array_key_exists('input', $_GET))
			{
				$input = $_GET['input'];
			}
			else
			{
				$input = null;
			}
			break;
		case "POST":
			if(array_key_exists('destroy', $_POST) === TRUE)
			{
				destroy_session();
			}
			if(array_key_exists('input', $_POST))
			{
				$input = $_POST['input'];
			}
			else
			{
				$input = null;
			}
			break;
	}

	if($input !== NULL)
	{
		$detected = check_for_number($input);

		if($detected === TRUE)
		{
			$_SESSION['containing']++;
		}
		elseif($detected === FALSE)
		{
			$_SESSION['not_containing']++;
		}
	}

	//Рендерим шаблон, и отдаем браузеру
	echo render("/views/index.php", [
		'detected' => $detected,
		'containing' => $_SESSION['containing'],
		'not_containing' => $_SESSION['not_containing'],
	]);
}

init_session();
check();