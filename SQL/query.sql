SELECT comp.name, quot.rate, quot.time
FROM company as comp
LEFT JOIN  (SELECT * FROM quotation ORDER BY time DESC) as quot 
ON comp.id = quot.id_company
GROUP BY quot.id_company